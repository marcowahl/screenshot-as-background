;;;; package.lisp

(defpackage #:screenshot-as-background
  (:use #:cl :stumpwm))

(in-package #:screenshot-as-background)
