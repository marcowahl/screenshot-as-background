;; screenshot-as-background.lisp --- Set a screenshot as background



;; Copyright © 2016 Marco Wahl

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.



;; Key binding suggestions:

;; (define-key *root-map* (kbd "C-s") "screenshoot-as-background")
;; (define-key *root-map* (kbd "C-b") "black-as-background")

;; With the typical key C-t as root-map entry one gets the bindings
;; C-ts and C-tb respectively.


(in-package #:screenshot-as-background)

(export
 '(black-as-background
   screenshoot-as-background))


(defcommand screenshoot-as-background () ()
  "Set current screenshot as background.
Save the screenshot under filename including the date."
  (let ((screenshot-file-name
         (format
          nil "~a~a~a" "/tmp/"
          (multiple-value-bind
                (sec min hou day mon yea)
              (get-decoded-time)
            (format nil "~d~2,'0d~2,'0d~2,'0d~2,'0d~2,'0d" yea mon day hou min sec))
          "-stumpwm-screenshot.png")))
    (screenshot:screenshot screenshot-file-name)
    (run-shell-command
     (format nil "xsetbg ~A" screenshot-file-name))))

(defcommand black-as-background () ()
  "Set background to black."
  (let ((screenshot-file-name "/tmp/black.png"))
    (run-shell-command
     (format nil "convert -size 1x1 xc:black ~A ; xsetbg ~A"
             screenshot-file-name
             screenshot-file-name))))

(defcommand white-as-background () ()
  "Set background to white."
  (let ((screenshot-file-name "/tmp/white.png"))
    (run-shell-command
     (format nil "convert -size 1x1 xc:white ~A ; xsetbg ~A"
             screenshot-file-name
             screenshot-file-name))))



;;;; screenshot-as-background.lisp ends here
