# What #

This module provides two functions for stumpwm.  The functionality is

1. Save the current desktop as background image.
   - Sidenote: Every screenshot gets saved under a name including the
     current time stamp.  See directory /tmp.
2. Set the background to black.

# Usage #

Call the function you like from stumpwm.  E.g. `C-t ; screenshoot-as-background`.

You can bind the functions to some keys in your stumpwm init-file.

# Install #

This is one possible install, there might be others.

- Copy this directory to ~/.stumpwm.d/modules.

- Add the line `(load-module "screenshot-as-background")` to your
  ~/.stumpwm.d/init.lisp.

  - Optional: Add further the lines

``` common-lisp
(define-key *root-map* (kbd "C-s") "screenshoot-as-background")
(define-key *root-map* (kbd "C-b") "black-as-background")
```

- That's all.

# Why #

- The screenshot on the background allows convenient comparisons.
- Simply get a history about what have been on the desktop.

# Dependencies #

- Stumpwm module screenshot.  See
  https://github.com/stumpwm/stumpwm-contrib/.
  - zpng lisp library.
- Imagemagick
- xsetbg
  - xloadimage

# License #

GPLv3.  See ./LICENSE.

# Contact #

Contact marcowahlsoft@gmail.com for any issues.
