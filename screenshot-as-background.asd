;;;; screenshot-as-background.asd

(asdf:defsystem #:screenshot-as-background
  :serial t
  :description "Set screenshot as background"
  :author "Marco Wahl <marcowahlsoft@gmail.com>"
  :license "GPLv3"
  :depends-on (#:stumpwm #:screenshot)
  :components ((:file "package")
               (:file "screenshot-as-background")))
